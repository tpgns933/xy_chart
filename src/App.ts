import Chart from './components/Chart';
import createChartOptions from './util/createChartOptions';
import { CHART_TYPE } from './constants';
import Sortable from 'sortablejs';

export default function App($root: HTMLDivElement) {
  //* new XYChart(DOM, type, chartOption);
  new Chart({ $root, type: CHART_TYPE.BAR, option: createChartOptions[CHART_TYPE.BAR]() });
  new Chart({ $root, type: CHART_TYPE.SCATTER, option: createChartOptions[CHART_TYPE.SCATTER]() });
  new Chart({ $root, type: CHART_TYPE.PIE, option: createChartOptions[CHART_TYPE.PIE]() });

  const init = () => {
    new Sortable($root, {});
    document.querySelectorAll('.chart-Container').forEach($chartBox => new Sortable($chartBox, { group: 'shared' }));
  };

  init();
}
