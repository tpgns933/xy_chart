export const CHART_WIDTH: string = '90%';

export const CHART_TYPE: {
  [key: string]: string;
} = {
  BAR: 'bar',
  SCATTER: 'scatter',
  PIE: 'pie',
};
