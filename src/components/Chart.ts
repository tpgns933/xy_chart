//* import Chart from 'chart.js/auto' => 200.1k
// import Chart from 'chart.js/auto';

// * 웹팩의 트리 쉐이킹 을 활용하여 용량 최적화
import {
  Chart,
  BarElement,
  BarController,
  CategoryScale,
  LinearScale,
  ArcElement,
  ScatterController,
  PointElement,
  PieController,
  Tooltip,
  Legend,
} from 'chart.js';

import { CHART_WIDTH } from '../constants';
import { chartParameterType } from '../types';

export default function XYChart({ $root, type, option }: chartParameterType) {
  this.chartContainer = document.createElement('article');
  this.chartContainer.classList.add('chart-Container');

  this.canvas = document.createElement('canvas');
  this.canvas.style.width = CHART_WIDTH;
  this.canvas.id = `${type}chart`;
  this.canvas.classList.add('chart');

  this.chartContainer.appendChild(this.canvas);
  $root.appendChild(this.chartContainer);

  Chart.register(
    BarElement,
    BarController,
    ArcElement,
    ScatterController,
    PointElement,
    PieController,
    CategoryScale,
    LinearScale,
    Tooltip,
    Legend
  );
  new Chart(this.canvas, option);
}
