import { dataType } from '../types';

const createData = () => {
  let data: dataType[] = [];

  for (let i = 0; i < 1000; i++) {
    data.push({
      x: Number((Math.random() * (100 - -100) + -100).toFixed(2)),
      y: Number((Math.random() * (100 - -100) + -100).toFixed(2)),
    });
  }

  return data;
};

export default createData;
