import createData from './createData';
import { dataType } from '../types';

export const findingAverage = (data: dataType[]) => {
  const averageData: dataType[] = [];

  for (let i = 0; i < 1000; i += 100) {
    const sumData = data
      .slice(i, i + 100)
      .reduce((pre, cur) => ({ x: pre.x + cur.x, y: pre.y + cur.y }), { x: 0, y: 0 });

    averageData.push({ x: Number((sumData.x / 100).toFixed(2)), y: Number((sumData.y / 100).toFixed(2)) });
  }

  return averageData;
};

const createChartOptions = {
  pie() {
    const data = createData();

    const topLeftCoordinate = data.filter(data => data.x < 0 && data.y >= 0);
    const topRightCoordinate = data.filter(data => data.x >= 0 && data.y >= 0);
    const bottomLeftCoordinate = data.filter(data => data.x < 0 && data.y < 0);
    const bottomRightCoordinate = data.filter(data => data.x >= 0 && data.y < 0);

    return {
      type: 'pie',
      data: {
        labels: ['+x +y', '+x -y', '-x -y', '-x +y'],
        datasets: [
          {
            label: '개수',
            data: [
              topRightCoordinate.length,
              bottomRightCoordinate.length,
              bottomLeftCoordinate.length,
              topLeftCoordinate.length,
            ],
            backgroundColor: ['#5532bd', '#6bb4dd', '#65b3bb', '#1d0059'],
            borderWidth: 2,
          },
        ],
      },
      options: {
        responsive: false,
        hoverBorderWidth: 6,
        hoverOffset: 20,
      },
    };
  },

  bar() {
    const data = createData();
    const averageData = findingAverage(data);

    return {
      type: 'bar',
      data: {
        labels: averageData.map((_, i) => `${i + 1}`),
        datasets: [
          {
            label: 'X value',
            data: averageData.map(data => data.x),
            borderColor: '#6bb4dd',
            backgroundColor: '#5532bd',
          },
          {
            label: 'Y value',
            data: averageData.map(data => data.y),
            borderColor: '#5532bd',
            backgroundColor: '#6bb4dd',
          },
        ],
      },
      options: {
        responsive: false,
        hoverBorderWidth: 2,
        hoverBorderRadius: 20,
      },
    };
  },

  scatter() {
    const data = createData();
    const averageData = findingAverage(data);

    return {
      type: 'scatter',
      data: {
        datasets: [
          {
            label: 'Scatter Dataset',
            data: averageData.map(data => ({ x: data.x, y: data.y })),
            backgroundColor: '#5532bd',
            radius: 5,
          },
        ],
      },
      options: {
        responsive: false,
        hoverRadius: 10,
      },
    };
  },
};

export default createChartOptions;
