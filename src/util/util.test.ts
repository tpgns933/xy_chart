import createData from './createData';
import createChartOptions, { findingAverage } from './createChartOptions';

describe('데이터 생성 함수 createData 테스트', () => {
  const data = createData();

  it('데이터 내부에 x, y 프로퍼티 확인 && 프로퍼티 값이 number 인지 확인 && 데이터 개수 확인 (1000개)', () => {
    expect(
      data.filter(item => item.x && item.y && typeof item.x === 'number' && typeof item.y === 'number')
    ).toHaveLength(1000);
  });
});

describe('findingAverage 함수 테스트', () => {
  const data = findingAverage(createData());

  it('findingAverage 함수 테스트 : 데이터 내부에 x, y 프로퍼티 확인 && 프로퍼티 값이 number 인지 확인 && 데이터 개수 확인 (10개)', () => {
    expect(
      data.filter(item => item.x && item.y && typeof item.x === 'number' && typeof item.y === 'number')
    ).toHaveLength(10);
  });
});

describe('createChartOptions 메서드 테스트', () => {
  it('createChartOptions.pie 메서드 리턴 값에 알맞은 타입이 지정돼있고 data와 options 프로퍼티가 있는지 확인', () => {
    expect(() => {
      const pieChartOption = createChartOptions.pie();
      return pieChartOption.type === 'pie' && pieChartOption.data && pieChartOption.options;
    }).toBeTruthy();
  });

  it('createChartOptions.bar 메서드 리턴 값에 알맞은 타입이 지정돼있고 data와 options 프로퍼티가 있는지 확인', () => {
    expect(() => {
      const pieChartOption = createChartOptions.bar();
      return pieChartOption.type === 'bar' && pieChartOption.data && pieChartOption.options;
    }).toBeTruthy();
  });

  it('createChartOptions.scatter 메서드 리턴 값에 알맞은 타입이 지정돼있고 data와 options 프로퍼티가 있는지 확인', () => {
    expect(() => {
      const pieChartOption = createChartOptions.scatter();
      return pieChartOption.type === 'scatter' && pieChartOption.data && pieChartOption.options;
    }).toBeTruthy();
  });
});
