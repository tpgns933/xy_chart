import {
  BubbleDataPoint,
  ChartConfiguration,
  ChartConfigurationCustomTypesPerDataset,
  ChartTypeRegistry,
  Point,
} from 'chart.js';

export type dataType = {
  x: number;
  y: number;
};

export type chartParameterType = {
  $root: HTMLDivElement;
  type: string;
  option:
    | ChartConfiguration<keyof ChartTypeRegistry, (number | [number, number] | Point | BubbleDataPoint)[], unknown>
    | ChartConfigurationCustomTypesPerDataset<
        keyof ChartTypeRegistry,
        (number | [number, number] | Point | BubbleDataPoint)[],
        unknown
      >;
};
